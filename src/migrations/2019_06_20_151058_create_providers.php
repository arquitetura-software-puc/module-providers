<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProviders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('providers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('legal_name');
            $table->string('trade_name');
            $table->string('email');
            $table->string('cnpj');
            $table->string('state_registration');
            $table->string('phone');
            $table->string('address');
            $table->integer('address_number');
            $table->string('address_complement')->nullable();
            $table->string('address_state');
            $table->string('address_city');
            $table->string('address_code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('providers');
    }
}
