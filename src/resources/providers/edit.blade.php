@extends('layouts.admin')
@section('content')
<div id="heading-breadcrumbs">
    <div class="container">
        <div class="row d-flex align-items-center flex-wrap">
            <div class="col-md-7">
                <h1 class="h2">Fornecedores</h1>
            </div>
            <div class="col-md-5">
                <ul class="breadcrumb d-flex justify-content-end">
                    <li class="breadcrumb-item"><a href="{{url('/admin')}}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Fornecedores</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="content">
    <div class="container">
        <div class="row bar">
            <div class="col-md-3">
                <!-- MENUS AND FILTERS-->
                <div class="panel panel-default sidebar-menu">
                    <div class="panel-heading">
                        <h3 class="h4 panel-title">Menu</h3>
                    </div>
                    <div class="panel-body">
                        @include('elements.admin_menu')
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <h2>Alterar Produto</h2>
                @include('elements.message_success_error')

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif
                <form method="POST" action="{{ route('fornecedor_atualizar') }}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id" value="{{$provider->id}}" />
                    <div class="form-group">    
                        <label for="trade_name">Nome Fantasia:</label>
                        <input type="text" class="form-control" id="trade_name" name="trade_name" value="{{$provider->trade_name}}"/>
                    </div>

                    <div class="form-group">    
                        <label for="legal_name">Razão Social:</label>
                        <input type="text" class="form-control" id="legal_name" name="legal_name" value="{{$provider->legal_name}}"/>
                    </div>

                    <div class="form-group">    
                        <label for="email">Email:</label>
                        <input type="text" class="form-control" id="email" name="email" value="{{$provider->email}}"/>
                    </div>                    

                    <div class="form-group">    
                        <label for="cnpj">CNPJ:</label>
                        <input type="text" class="form-control" id="cnpj" name="cnpj" value="{{$provider->cnpj}}"/>
                    </div>
                    
                    <div class="form-group">    
                        <label for="state_registration">Inscrição Estadual:</label>
                        <input type="text" class="form-control" id="state_registration" name="state_registration" value="{{$provider->state_registration}}"/>
                    </div>                    

                    <div class="form-group">    
                        <label for="phone">Telefone:</label>
                        <input type="text" class="form-control" id="phone" name="phone" value="{{$provider->phone}}"/>
                    </div>                    
                    
                    <div class="form-group">    
                        <label for="address">Logradouro:</label>
                        <input type="text" class="form-control" id="address" name="address" value="{{$provider->address}}"/>
                    </div>

                    <div class="form-group">    
                        <label for="address_number">Número:</label>
                        <input type="text" class="form-control" id="address_number" name="address_number" value="{{$provider->address_number}}"/>
                    </div>
                    
                    <div class="form-group">    
                        <label for="address_complement">Complemento:</label>
                        <input type="text" class="form-control" id="address_complement" name="address_complement" value="{{$provider->address_complement}}"/>
                    </div>

                    <div class="form-group">    
                        <label for="address_city">Cidade:</label>
                        <input type="text" class="form-control" id="address_city" name="address_city" value="{{$provider->address_city}}"/>
                    </div>                    

                    <div class="form-group">    
                        <label for="address_state">Estado:</label>
                        <input type="text" class="form-control" id="address_state" name="address_state" value="{{$provider->address_state}}"/>
                    </div>
                    
                    <div class="form-group">    
                        <label for="address_code">CEP:</label>
                        <input type="text" class="form-control" id="address_code" name="address_code" value="{{$provider->address_code}}"/>
                    </div>                          
                    <button type="submit" class="btn btn-default">Alterar</button>
                </form>
            </div>                
        </div>
    </div>
</div>
</div>
<!-- GET IT-->
@endsection

@section('script')
<script type="text/javascript">
    $('[name="cnpj"]').mask('00.000.000/0000-00');
    $('[name="address_code"]').mask('00000-000');
    $('[name="phone"]').mask('(00) 0000-00009');
</script>
@endsection('script')