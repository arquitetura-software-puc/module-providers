@extends('layouts.admin')
@section('content')
<div id="heading-breadcrumbs">
    <div class="container">
        <div class="row d-flex align-items-center flex-wrap">
            <div class="col-md-7">
                <h1 class="h2">Fornecedores</h1>
            </div>
            <div class="col-md-5">
                <ul class="breadcrumb d-flex justify-content-end">
                    <li class="breadcrumb-item"><a href="{{url('/admin')}}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Fornecedores</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="content">
    <div class="container">
        <div class="row bar">
            <div class="col-md-3">
                <!-- MENUS AND FILTERS-->
                <div class="panel panel-default sidebar-menu">
                    <div class="panel-heading">
                        <h3 class="h4 panel-title">Menu</h3>
                    </div>
                    <div class="panel-body">
                        @include('elements.admin_menu')
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                @include('elements.message_success_error')
                <a href="{{url('/fornecedor/novo/')}}" class="btn btn-sm btn-template-main float-right">Novo fornecedor</a>
                <br /><br />
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nome Fantasia</th>
                                <th>Razão Social</th>
                                <th>Telefone</th>
                                <th>Cadastro</th>
                                <th>Alteração</th>
                                <th>Ação</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($listProviders as $provider)
                            <tr>
                                <td>{{$provider->id}}</td>                                    
                                <td>{{$provider->trade_name}}</td>
                                <td>{{$provider->legal_name}}</td>
                                <td>{{$provider->phone}}</td>
                                <td>{{$provider->created_at->format('d/m/Y')}}</td>
                                <td>{{$provider->updated_at->format('d/m/Y')}}</td>
                                <td class="actions">
                                    <div class="btn-group">
                                        <a href="{{url('/fornecedor/editar/' . $provider->id)}}" class="btn btn-info btn-sm" title="Editar"><i class="fa fa-pencil fa-align-center"></i></a>                                        
                                        <form name="post_{{$provider->id}}" style="display:none;" method="post" action="{{url('/fornecedor/excluir/')}}">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$provider->id}}">
                                        </form>
                                        <a href="#" class="btn btn-danger btn-sm" title="Excluir" onclick="if (confirm('Tem certeza que deseja excluir o registro # {{$provider->id}}?')) {
                                                    document.post_{{$provider->id}}.submit();
                                                }
                                                event.returnValue = false;
                                                return false;">
                                            <i class="fa fa-trash fa-align-center"></i>
                                        </a>                                   
                                    </div>                                
                                </td>                                        
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                    {{ $listProviders->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- GET IT-->
@endsection
