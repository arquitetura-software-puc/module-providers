@extends('layouts.admin')
@section('content')
<div id="heading-breadcrumbs">
    <div class="container">
        <div class="row d-flex align-items-center flex-wrap">
            <div class="col-md-7">
                <h1 class="h2">Fornecedores</h1>
            </div>
            <div class="col-md-5">
                <ul class="breadcrumb d-flex justify-content-end">
                    <li class="breadcrumb-item"><a href="{{url('/admin')}}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Fornecedores</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="content">
    <div class="container">
        <div class="row bar">
            <div class="col-md-3">
                <!-- MENUS AND FILTERS-->
                <div class="panel panel-default sidebar-menu">
                    <div class="panel-heading">
                        <h3 class="h4 panel-title">Menu</h3>
                    </div>
                    <div class="panel-body">
                        @include('elements.admin_menu')
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <h2>Cadastrar Fornecedor</h2>
                @include('elements.message_success_error')

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif
                <form method="post" action="{{ route('fornecedor_salvar') }}">
                    @csrf
                    <div class="form-group">    
                        <label for="trade_name">Nome Fantasia:</label>
                        <input type="text" class="form-control" id="trade_name" name="trade_name" value="{{old('trade_name')}}"/>
                    </div>

                    <div class="form-group">    
                        <label for="legal_name">Razão Social:</label>
                        <input type="text" class="form-control" id="legal_name" name="legal_name" value="{{old('legal_name')}}"/>
                    </div>

                    <div class="form-group">    
                        <label for="email">Email:</label>
                        <input type="text" class="form-control" id="email" name="email" value="{{old('email')}}"/>
                    </div>                    

                    <div class="form-group">    
                        <label for="cnpj">CNPJ:</label>
                        <input type="text" class="form-control" id="cnpj" name="cnpj" value="{{old('cnpj')}}"/>
                    </div>
                    
                    <div class="form-group">    
                        <label for="state_registration">Inscrição Estadual:</label>
                        <input type="text" class="form-control" id="state_registration" name="state_registration" value="{{old('state_registration')}}"/>
                    </div>                    

                    <div class="form-group">    
                        <label for="phone">Telefone:</label>
                        <input type="text" class="form-control" id="phone" name="phone" value="{{old('phone')}}"/>
                    </div>                    
                    
                    <div class="form-group">    
                        <label for="address">Logradouro:</label>
                        <input type="text" class="form-control" id="address" name="address" value="{{old('address')}}"/>
                    </div>

                    <div class="form-group">    
                        <label for="address_number">Número:</label>
                        <input type="text" class="form-control" id="address_number" name="address_number" value="{{old('address_number')}}"/>
                    </div>
                    
                    <div class="form-group">    
                        <label for="address_complement">Complemento:</label>
                        <input type="text" class="form-control" id="address_complement" name="address_complement" value="{{old('address_complement')}}"/>
                    </div>

                    <div class="form-group">    
                        <label for="address_city">Cidade:</label>
                        <input type="text" class="form-control" id="address_city" name="address_city" value="{{old('address_city')}}"/>
                    </div>                    

                    <div class="form-group">    
                        <label for="address_state">Estado:</label>
                        <input type="text" class="form-control" id="address_state" name="address_state" value="{{old('address_state')}}"/>
                    </div>
                    
                    <div class="form-group">    
                        <label for="address_code">CEP:</label>
                        <input type="text" class="form-control" id="address_code" name="address_code" value="{{old('address_code')}}"/>
                    </div>                    
                    
                    <button type="submit" class="btn btn-default">Cadastrar</button>
                </form>
            </div>                
        </div>
    </div>
</div>
</div>
<!-- GET IT-->
@endsection
