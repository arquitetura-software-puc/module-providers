<?php

namespace Mgzaspuc\Providers\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'legal_name' => 'required|unique:providers|max:191',
            'trade_name' => 'required|unique:providers|max:191',
            'email' => 'required|email|unique:providers|max:191',
            'cnpj' => 'required|unique:providers|max:191',
            'state_registration' => 'required|unique:providers|max:191',
            'phone' => 'required|unique:providers|max:191',
            'address' => 'required|max:191',
            'address_number' => 'required|max:191',
            'address_complement' => 'max:191',
            'address_state' => 'required|max:191',
            'address_city' => 'required|max:191',
            'address_code' => 'required|max:191',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'legal_name.required' => 'O campo Razão Social do Fornecedor não pode ser vazio',
            'legal_name.unique' => 'A Razão Social do Fornecedor já existe cadastrado no sistema',
            'legal_name.max' => 'A Razão Social do Fornecedor não pode ter mais de 191 caracteres',
            'trade_name.required' => 'O campo Nome Fantasia do Fornecedor não pode ser vazio',
            'trade_name.unique' => 'O Nome Fantasia do Fornecedor já existe cadastrado no sistema',
            'trade_name.max' => 'O Nome Fantasia do Fornecedor não pode ter mais de 191 caracteres',            
            'email.required' => 'O campo Email não pode ser vazio',
            'email.email' => 'O Email informado é inválido',
            'email.unique' => 'O Email já existe cadastrado no sistema',
            'email.max' => 'O Email não pode ter mais de 191 caracteres',                        
            'cnpj.required' => 'O campo CNPJ não pode ser vazio',
            'cnpj.unique' => 'O CNPJ já existe cadastrado no sistema',
            'cnpj.max' => 'O CNPJ não pode ter mais de 191 caracteres',              
            'state_registration.required' => 'O campo Inscrição Estadual não pode ser vazio',
            'state_registration.unique' => 'A Inscrição Estadual já existe cadastrado no sistema',
            'state_registration.max' => 'A Inscrição Estadual não pode ter mais de 191 caracteres',                 
            'phone.required' => 'O campo Telefone não pode ser vazio',
            'phone.unique' => 'O Telefone já existe cadastrado no sistema',
            'phone.max' => 'O Telefone não pode ter mais de 191 caracteres',                             
            'address.required' => 'O campo Logradouro não pode ser vazio',            
            'address.max' => 'O Logradouro não pode ter mais de 191 caracteres',              
            'address_number.required' => 'O campo Número não pode ser vazio',            
            'address_number.max' => 'O Número não pode ter mais de 191 caracteres',               
            'address_complement.max' => 'O Complemento não pode ter mais de 191 caracteres',              
            'address_state.required' => 'O campo Estado não pode ser vazio',            
            'address_state.max' => 'O Estado não pode ter mais de 191 caracteres',                          
            'address_city.required' => 'O campo Cidade não pode ser vazio',            
            'address_city.max' => 'A Cidade não pode ter mais de 191 caracteres',                                      
            'address_code.required' => 'O campo CEP não pode ser vazio',            
            'address_code.max' => 'O CEP não pode ter mais de 191 caracteres',               
        ];
    }
}
