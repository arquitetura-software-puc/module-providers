<?php

namespace Mgzaspuc\Providers\Http\Controllers;

use Mgzaspuc\Providers\Http\Requests\StoreRequest;
use Mgzaspuc\Providers\Http\Requests\UpdateRequest;
use Illuminate\Http\Request;
use Mgzaspuc\Providers\Providers;
use App\Http\Controllers\Controller;

class ProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $providers = new Providers();
        $listProviders = $providers->paginate(15);
        
        return view('providers.index', compact('listProviders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provider = new Providers();
        $listProvider = $provider->all();
        return view('providers.create', compact('listProvider'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        try{        
            $provider = new Providers();
            $provider->legal_name = $request->get('legal_name');
            $provider->trade_name = $request->get('trade_name');
            $provider->email = $request->get('email');
            $provider->cnpj = $request->get('cnpj');
            $provider->state_registration = $request->get('state_registration');
            $provider->phone = $request->get('phone');
            $provider->address = $request->get('address');
            $provider->address_number = $request->get('address_number');
            $provider->address_complement = $request->get('address_complement');
            $provider->address_state = $request->get('address_state');
            $provider->address_city = $request->get('address_city');
            $provider->address_code = $request->get('address_code');
            $provider->created_at = new \DateTime();
            $provider->updated_at = new \DateTime();
            $provider->save();
        
            return redirect('/fornecedores')
                ->with('success', 'Registro criado com sucesso!');            
            
        } catch (\Exception $ex) {
            return redirect('/fornecedor/novo')
                ->with('error', 'Não foi possível criar o registro!')
                ->withInput($request->input());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $providers = new Providers();
        $provider = $providers->find($id);
        
        return view('providers.edit', compact('provider'));    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request)
    {
        try{
            $providers = new Providers();
            $provider = $providers->find($request->input('id'));            
            $provider->legal_name = $request->get('legal_name');
            $provider->trade_name = $request->get('trade_name');
            $provider->email = $request->get('email');
            $provider->cnpj = $request->get('cnpj');
            $provider->state_registration = $request->get('state_registration');
            $provider->phone = $request->get('phone');
            $provider->address = $request->get('address');
            $provider->address_number = $request->get('address_number');
            $provider->address_complement = $request->get('address_complement');
            $provider->address_state = $request->get('address_state');
            $provider->address_city = $request->get('address_city');
            $provider->address_code = $request->get('address_code');
            $provider->updated_at = new \DateTime();
            $provider->save();
        
            return redirect('/fornecedores')
                ->with('success', 'Registro alterado com sucesso!');            
            
        } catch (\Exception $ex) {
            return redirect('/fornecedor/editar/' . $request->input('id'))
                ->with('error', 'Não foi possível alterar o registro!');
        }
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            $providers = new Providers();
            $provider = $providers->find($request->input('id'));        
            $provider->delete();       
            
            return redirect('/fornecedores')
                ->with('success', 'Registro excluído com sucesso!');            
            
        } catch (\Exception $ex) {
            return redirect('/fornecedores')
                ->with('error', 'Não foi possível excluir o registro!');
        }
    }
}
