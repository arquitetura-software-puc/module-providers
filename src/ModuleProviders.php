<?php

namespace Mgzaspuc\Providers;

use Illuminate\Support\ServiceProvider;

class ModuleProviders extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/migrations' => base_path('database/migrations/')
        ]);

        $this->publishes([
            __DIR__.'/resources' => base_path('resources/views/')
        ]);
    }
}