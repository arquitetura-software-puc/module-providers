<?php

namespace Mgzaspuc\Providers;

use Illuminate\Database\Eloquent\Model;

class Providers extends Model
{
    protected $table = 'providers';

    public function products() {
        return $this->hasMany('Mgzaspuc\Products\Products','id_provider','id');
    }
}
